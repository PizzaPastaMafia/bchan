<?php
require('./config.php');
require('../vendor/autoload.php');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Authorization');
header('Access-Control-Allow-Methods:  POST, PUT');
header('Content-Type: application/json');


$conn = newAdoConnection('mysqli');
$conn->connect($dbHost, $dbUsername, $dbPassword, $dbName);

// Set the response content type to JSON
header('Content-Type: application/json');

// Get the board name from the query string
$threadId = $_GET['threadId'];

$sql = "
    SELECT *
    FROM threads
    WHERE board_code = '$threadId'
";

$result = $conn->Execute($sql);
// Check for SQL errors
if ($result === false) {
    echo(error_log("SQL error: ".$conn->ErrorMsg()));
    die("SQL error: ".$conn->ErrorMsg());
}

$thread = array(
    'thread_title' => $result->fields['thread_title'],
    'thread_text' => $result->fields['thread_text']
);

// Return the result as JSON
echo json_encode($thread);

// Close the database connection
$conn->Close();

?>