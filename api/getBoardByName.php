<?php
require('./config.php');
require('../vendor/autoload.php');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Authorization');
header('Access-Control-Allow-Methods:  POST, PUT');
header('Content-Type: application/json');


$conn = newAdoConnection('mysqli');
$conn->connect($dbHost, $dbUsername, $dbPassword, $dbName);

// Set the response content type to JSON
header('Content-Type: application/json');

// Get the board name from the query string
$boardCode = $_GET['boardCode'];

$sql = "
    SELECT *
    FROM boards
    WHERE board_code = '$boardCode'
";

$result = $conn->Execute($sql);
// Check for SQL errors
if ($result === false) {
    echo(error_log("SQL error: ".$conn->ErrorMsg()));
    die("SQL error: ".$conn->ErrorMsg());
}

$board = array(
    'board_id' => $result->fields['board_id'],
    'board_name' => $result->fields['board_name'],
    'board_code' => $result->fields['board_code']
);

// Return the result as JSON
echo json_encode($board);

// Close the database connection
$conn->Close();

?>