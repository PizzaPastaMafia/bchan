<?php

require('./config.php');
require('../vendor/autoload.php');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Authorization');
header('Access-Control-Allow-Methods:  POST, PUT');
header('Content-Type: application/json');


$conn = newAdoConnection('mysqli');
$conn->connect($dbHost, $dbUsername, $dbPassword, $dbName);

// Set the response content type to JSON
header('Content-Type: application/json');

// Get the board ID and number of threads to display from the query string
$boardId = $_GET['boardId'];
$numThreads = $_GET['numThreads'];


// Build the SQL query to retrieve the most recent threads for the selected board
$sql = "
  SELECT thread_id, thread_title, thread_text
  FROM threads
  WHERE board_id = $boardId
  ORDER BY thread_id DESC
  LIMIT $numThreads
";

// Execute the query and retrieve the results
$result = $conn->Execute($sql);

// Check for SQL errors
if ($result === false) {
  error_log("SQL error: ".$conn->ErrorMsg());
  die("SQL error: ".$conn->ErrorMsg());
}

// Convert the result set to an associative array
$threads = array();
while (!$result->EOF) {
  $threads[] = array(
    'thread_id' => $result->fields['thread_id'],
    'thread_title' => $result->fields['thread_title'],
    'thread_text' => $result->fields['thread_text']
  );
  $result->MoveNext();
}

// Return the result as JSON
echo json_encode($threads);

// Close the database connection
$conn->Close();

?>